setInterval(() => {
	localStorage.setItem("saveForm1", document.querySelector("#form-name").value);
	localStorage.setItem("saveForm2", document.querySelector("#form-mail").value);
	localStorage.setItem("saveForm3", document.querySelector("#form-text").value);
}, 500);


// открыть по кнопке
$('.button-main').click(function () {
	$('.popup').fadeIn();
	$('.popup').addClass('open');
	history.pushState({ page: 2 }, null, "formOpen");
	document.querySelector("#form-name").value = localStorage.getItem("saveForm1");
	document.querySelector("#form-mail").value = localStorage.getItem("saveForm2");
	document.querySelector("#form-text").value = localStorage.getItem("saveForm3");
});
// закрыть на крестик
$('.popup__close').click(function () {
	hidePopup();
});

$(window).on("popstate", function () {
	hidePopup();
});

// закрыть по клику вне окна
$(document).mouseup(function (e) {
	var popup = $('.popup__content');
	if (e.target != popup[0] && popup.has(e.target).length === 0) {
		hidePopup();
	}
});

$('.button').click(function () {
	localStorage.clear();
});

function hidePopup() {
	$('.popup').removeClass('open');
	history.pushState({ page: 1 }, null, "./")
}

history.replaceState({ page: null }, "Default state", "./");

